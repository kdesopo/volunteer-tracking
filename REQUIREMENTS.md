# Project Requirements

## User Sign In and Sign up
- The user should be able to sign in with the following social accounts:
  - Google
  - Facebook
- The application will track the name, email address from the social accounts, with the user provider information. 
  - This should be designed in a way that the user can sign in from multiple social accounts. For example, if they signed in from Facebook and they use Google sign in with the same email address, the sign in will be tied to the same user.

## Navigation
The application will have two distinct sections:

- User section
  - In this section the user will be able to view:
    - User profile
    - User Feed
    - Search for events
    - Register for events
    - View past and future events
- Organization section
  - This section will be linked from the user section for admins of organization
  - The user will be able to manage the organization and its current and future events and volunteers.

## User Feed
- This will be the main page for the application.
- This will have a “feed” for signed up events and any upcoming events from organizations they have previously volunteered at.



### Check in View
- This would be for all current events, where the organizer can see the list of attending volunteers and allow the organizer to check in these attendees
- Have the ability to check in volunteer that doesn’t have a user profile yet.
- We can send the new volunteer an email with information to sign up for the application
- Creating Events
  - As an organization administrator, you can create “volunteer events”
  - These events would have the following:
    - Title
    - Description
    - Start Date
    - End Date
    - Optionally, you can provide:
    - Location
    - Set of slots or responsibilities
    - Limit on number of attendees
    - Limit on number of attendees per role
