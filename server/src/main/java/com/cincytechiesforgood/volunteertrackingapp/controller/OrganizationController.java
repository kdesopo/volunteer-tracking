package com.cincytechiesforgood.volunteertrackingapp.controller;

import com.cincytechiesforgood.volunteertrackingapp.model.Organization;
import com.cincytechiesforgood.volunteertrackingapp.repository.OrganizationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/organizations")
public class OrganizationController {

    private final OrganizationRepository organizationRepository;

    @Autowired
    public OrganizationController(OrganizationRepository organizationRepository) {
        this.organizationRepository = organizationRepository;
    }

    @GetMapping
    public Page<Organization> getOrganizations(Pageable pageable) {
        return organizationRepository.findAll(pageable);
    }

    @PostMapping
    public Organization postOrganization(@Valid @RequestBody Organization organization) {
        return organizationRepository.save(organization);
    }

}
