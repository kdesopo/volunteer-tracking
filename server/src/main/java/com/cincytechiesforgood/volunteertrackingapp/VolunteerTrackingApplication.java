package com.cincytechiesforgood.volunteertrackingapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;


@SpringBootApplication
@EnableJpaAuditing
public class VolunteerTrackingApplication {

    public static void main(String[] args) {
        SpringApplication.run(VolunteerTrackingApplication.class, args);
    }

}
