package com.cincytechiesforgood.volunteertrackingapp.repository;

import com.cincytechiesforgood.volunteertrackingapp.model.Organization;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrganizationRepository extends JpaRepository<Organization, Long> {
}
