# Overview

This project serves as the REST API server for the Volunteer Tracking App.

# Getting Started

* Install the JDK from [here](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
* Install the Postgresql v11.2 from [here](https://www.enterprisedb.com/downloads/postgres-postgresql-downloads)
* Install Intell
* Set the password for postgres and use that in the application.yml folder

## Running PostgreSQL without installing
Using Docker, you can start a container running postgres without having to install it on your machine.
1. Install Docker. You can read how to install it in the [documentation, here](https://docs.docker.com/install/).
2. Run `docker run --rm --name local-postgres -it -e POSTGRES_PASSWORD=password -p 5432:5432 -d postgres:11`
3. The container will be running in the background, and a postgres database will be exposed on port 5432 on your local machine.
4. To stop the container, run `docker kill local-postgres`. The `--rm` in the command above will automatically delete the container after it is killed.

#Running the backend application
1. for MacOS users, the chmod +x mvnw.cmd needs to be run before the second command can be run.
2. open the terminal, and run .\mvnw.cmd clean install (necessary if using intellij community edition)
3. Debug from the main method in the VolunteerTrackingApplication

### Guides
The following guides illustrate how to use some features concretely:

* [Accessing Data with JPA](https://spring.io/guides/gs/accessing-data-jpa/)
* [Building a RESTful Web Service](https://spring.io/guides/gs/rest-service/)
* [Serving Web Content with Spring MVC](https://spring.io/guides/gs/serving-web-content/)
* [Building REST services with Spring](https://spring.io/guides/tutorials/bookmarks/)
